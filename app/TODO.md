# TODO
- [x] Team name is sent - After team name suggested a message is sent saying under consideration
  and until approval or rejection nothing else would be considered.
- Approval
    - [ ] Ask for conformation.
    - NO
        - [ ] Send message saying message said at start &rarr; Go back to first step.
    - YES
        - [ ] Send them message saying name is locked and also lock updates
- Reject
    - [ ] Send Rejection message.
    - [ ] Send message saying message said at start &rarr; Go back to first step.
