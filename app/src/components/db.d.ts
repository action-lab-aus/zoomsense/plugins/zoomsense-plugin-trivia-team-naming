import { Teams } from "./team";
import { NameSubmissions } from "./types";

declare module "@zoomsense/zoomsense-firebase" {
  interface ZoomSenseDataPlugins {
    teamPlugin: {
      [meetingId: string]: Teams;
    };
    teamNaming: {
      [meetingId: string]: NameSubmissions;
    };
  }
}
