export type NameSubmission = {
  name: string;
  approved: boolean;
};

export type NameSubmissions = { [teamId: string]: NameSubmission };
