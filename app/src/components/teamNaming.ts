import { isTeam, Teams } from "./team";
// Request that sensor with ID `sensorId` broadcasts the given message
// (i.e. sends it to everyone in the room)
const broadcastMessage = async (
  meetingId: string,
  sensorId: string,
  content: string,
  db: any
): Promise<void> => {
  console.log("messaging");
  await db.ref(`data/chats/${meetingId}/${sensorId}/message`).push({
    msg: content,
    receiver: 0,
  });
};

const getValFromDb = async (path: string, db: any): Promise<unknown> =>
  (await db.ref(path).once("value")).val();

const setValInDb = async (path: string, val: unknown, db: any): Promise<void> =>
  await db.ref(path).set(val);

const getAllBots = async (meetingId: string, db: any): Promise<string[]> => {
  const activeSpeakers = await getValFromDb(`/data/chats/${meetingId}`, db);

  if (!activeSpeakers || typeof activeSpeakers !== "object") return [];

  return Object.entries(
    (activeSpeakers as Record<string, { isInBO: boolean }>) ?? {}
  )
    .filter(([, { isInBO }]) => isInBO)
    .map(([zoomSensor]) => zoomSensor);
};

const getBotForTeam = async (
  meetingId: string,
  teamId: string,
  db: any
): Promise<string | null> => {
  const teamSensor = await getValFromDb(
    `data/plugins/teamPlugin/${meetingId}/${teamId}`,
    db
  );
  if (!isTeam(teamSensor)) {
    return null;
  }
  return teamSensor.sensorId;
};

export const sendNamingMessageToAllTeams = async (
  meetingId: string,
  db: any
) => {
  // message all other teams  the correct answer
  console.log("meetingId", meetingId);
  const botIds = await getAllBots(meetingId, db);
  console.log("botIds", botIds);

  for (const id of botIds) {
    console.log("id", id);
    await broadcastMessage(
      meetingId,
      id,
      "Please choose a team name and have one person submit it as a message.",
      db
    );
  }
};

export const approveTeamName = async (
  meetingId: string,
  teamId: string,
  name: string,
  db: any
) => {
  //update the value of approved in the database
  console.log(name);
  console.log(teamId);
  await setValInDb(
    `data/plugins/teamNaming/${meetingId}/${teamId}`,
    { name, approved: true },
    db
  );

  console.log("value set");
  const sensorId = await getBotForTeam(meetingId, teamId, db);
  if (!sensorId) return;
  broadcastMessage(
    meetingId,
    sensorId,
    "Your team name has been approved! You can relax until the trivia starts.",
    db
  );
  console.log(
    await getValFromDb(
      `data/plugins/teamPlugin/${meetingId}/${teamId}/teamName/`,
      db
    )
  );

  await setValInDb(
    `data/plugins/teamPlugin/${meetingId}/${teamId}/teamName/`,
    name,
    db
  );
};

export const rejectTeamName = async (
  meetingId: string,
  teamId: string,
  name: string,
  db: any
) => {
  console.log(name);
  console.log(teamId);

  //resetting team name to the team id
  await setValInDb(
    `data/plugins/teamNaming/${meetingId}/${teamId}`,
    { name: teamId, approved: false },
    db
  );

  console.log("value set");
  const sensorId = await getBotForTeam(meetingId, teamId, db);
  if (!sensorId) return;
  broadcastMessage(
    meetingId,
    sensorId,
    "Unfortunately your team name has been rejected. Please submit another name",
    db
  );
};

export const finaliseTeamNames = async (
  meetingId: string,
  teams: Teams | undefined,
  db: any
) => {
  if(!teams) return;

  for(const teamId of Object.keys(teams)){
    const teamName = await getValFromDb(
        `data/plugins/teamPlugin/${meetingId}/${teamId}/teamName/`,
        db
    );
    const sensorId = await getBotForTeam(meetingId, teamId, db);
    if (!sensorId) return;
    await broadcastMessage(
        meetingId,
        sensorId,
        `Your team name is ${teamName}`,
        db
    );
  }
};
