const admin = require("firebase-admin");
!admin.apps.length ? admin.initializeApp() : admin.app();

const { teamNaming } = require("./lib/index");

exports.teamNaming = teamNaming;
