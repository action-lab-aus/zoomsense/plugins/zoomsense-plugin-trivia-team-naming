import * as functions from "firebase-functions";
import {
  broadcastMessage,
  getBotForTeam,
  getPlayersTeam,
  getValFromDb,
  setValInDb,
} from "./db";
import { isNameSubmission } from "./types/team";
//import { broadcastMessage, getBotForTeam } from "./db";

export const teamNaming = functions.database
  .ref("/data/chats/{meetingId}/{sensor}/{chatId}")
  .onCreate(async (snapshot, context) => {
    const meetingId = context.params.meetingId;
    const { msg: messageContent, msgSenderName } = snapshot.val();

    if (!messageContent || !msgSenderName) return;

    const teamNamingConfig = await getValFromDb(
      `config/${meetingId}/current/currentState/plugins/teamnaming`
    );

    if (!teamNamingConfig) {
      return;
    }

    //getting the senders team
    const teamId = await getPlayersTeam(msgSenderName, meetingId);
    console.log("player team");
    console.log(teamId);
    console.log("after team id");
    //return if the zoomsensor sent the message -
    //TO DO: THIS DOESN'T WORK
    if (!teamId) return;

    const botId = await getBotForTeam(meetingId, teamId);
    console.log(botId);
    if (!botId) return;

    //seeing if the name is already approved
    const existingName = await getValFromDb(
      `data/plugins/teamNaming/${meetingId}/${teamId}`
    );
    if (existingName != null) {
      const isName = isNameSubmission(existingName);
      if (!isName) {
        return;
      }
      if (isName) {
        if (existingName.approved) {
          broadcastMessage(
            meetingId,
            botId,
            "You name has already been approved. You can no longer change it."
          );
          return;
        }
      }
    }
    console.log("setting value");
    await setValInDb(`data/plugins/teamNaming/${meetingId}/${teamId}`, {
      name: messageContent,
      approved: false,
    });
  });
