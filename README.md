# Trivia Team Naming Plugin

This plugin supports trivia teams naming themselves.

Teams submit their names to ZoomSensors, and the participants running the trivia night can moderate the names
from the Dashboard.
